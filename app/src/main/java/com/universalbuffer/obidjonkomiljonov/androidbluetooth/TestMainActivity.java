package com.universalbuffer.obidjonkomiljonov.androidbluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by leocordius on 24/05/2017.
 */

public class TestMainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter;
    private UUID uuid;
    Button b;
    private String uuidStr;
    public static final String pref = "UniversalBuffer";

    BackgroundThread backgroundThread;
    SwitchCompat switchCompat;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(pref, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();


        b = (Button) findViewById(R.id.sendButton);
        switchCompat = (SwitchCompat) findViewById(R.id.switchService);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this,
                    "Bluetooth doest not support",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        uuid = UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677");
        uuidStr = uuid.toString();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this,
                    "Bluetooth is not supported on this hardware platform",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        String stInfo = bluetoothAdapter.getName() + "\n" +
                bluetoothAdapter.getAddress();

        Toast.makeText(this, stInfo, Toast.LENGTH_SHORT).show();
        boolean isAlive = sharedpreferences.getBoolean("isAlive", true);
        if (isAlive) {
            startService(new Intent(this, ClipBoardService.class));
        }
        switchCompat.setChecked(isAlive);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putBoolean("isAlive", true);
                    editor.apply();
                    startService(new Intent(getApplicationContext(), ClipBoardService.class));
                    Toast.makeText(TestMainActivity.this, "Service is On", Toast.LENGTH_SHORT).show();
                } else {
                    editor.putBoolean("isAlive", false);
                    editor.apply();
                    stopService(new Intent(getApplicationContext(), ClipBoardService.class));
                    Toast.makeText(TestMainActivity.this, "Service is Off", Toast.LENGTH_SHORT).show();
                }
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivityForResult(intent, 0);
            }
        });

        // Get intent, action and MIME type
        Intent myintent = getIntent();
        String myaction = myintent.getAction();
        String mytype = myintent.getType();

        if (Intent.ACTION_SEND.equals(myaction) && mytype != null) {
            if (mytype.startsWith("image/")) {
                handleSendImage(myintent); // Handle single image being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }


    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
            Set<BluetoothDevice> pairDevices = bluetoothAdapter.getBondedDevices();

            //페어링된 장치가 있으면
            if (pairDevices.size() > 0) {
                for (BluetoothDevice device : pairDevices) {
                    //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                    //b.setText(device.getName().toString());

                    try {
                        final BluetoothSocket mysocket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("8dd28d12-863e-40ea-9f06-095a1a0a5648"));
                        mysocket.connect();
                        OutputStream outputStream = mysocket.getOutputStream();

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                        byte[] bytes = baos.toByteArray();

                        outputStream.write(bytes);
                        outputStream.flush();
                        mysocket.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

        startService();

    }

    private void startService() {
        Toast.makeText(this, "Start Service", Toast.LENGTH_SHORT).show();
        backgroundThread = new BackgroundThread();
        backgroundThread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (backgroundThread != null) {
            backgroundThread.cancel();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                startService();
            } else {
                Toast.makeText(this,
                        "BlueTooth NOT enabled",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private class BackgroundThread extends Thread {

        private BluetoothServerSocket bluetoothServerSocket = null;

        public BackgroundThread() {

        }

        @Override
        public void run() {
            Set<BluetoothDevice> pairDevices = bluetoothAdapter.getBondedDevices();

            //페어링된 장치가 있으면
            if (pairDevices.size() > 0) {
                for (BluetoothDevice device : pairDevices) {
                    //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                    //b.setText(device.getName().toString());

                    try {
                        final BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                        socket.connect();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                b.setText(socket.getRemoteDevice().getName());
                            }
                        });
                        socket.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //페어링된 장치가 없으면
            else {
                //기본 텍스트로 버튼 텍스트를 변경한다.
                try {
                    b.setText(R.string.search);
                } catch (Exception e) {
                    // TODO: Handle exception
                }

            }
        }

        public void cancel() {

            Toast.makeText(getApplicationContext(),
                    "close bluetoothServerSocket",
                    Toast.LENGTH_LONG).show();

            try {
                if (bluetoothServerSocket != null) {
                    bluetoothServerSocket.close();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
