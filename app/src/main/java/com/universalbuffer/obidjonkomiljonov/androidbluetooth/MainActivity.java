package com.universalbuffer.obidjonkomiljonov.androidbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    BluetoothAdapter mBlueToothAdapter;
    Button b;
    OutputStream outputStream;
    Thread th;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
        b = (Button) findViewById(R.id.sendButton);


        new Thread(runnable).start();

        ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        clipBoard.addPrimaryClipChangedListener(new ClipboardListener());

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                ConnectCheck();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    };


    private class ClipboardListener implements
            ClipboardManager.OnPrimaryClipChangedListener {
        public void onPrimaryClipChanged() {
            ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            CharSequence pasteData = "";
            ClipData.Item item = clipBoard.getPrimaryClip().getItemAt(0);
            pasteData = item.getText();
            Toast.makeText(getApplicationContext(), pasteData,
                    Toast.LENGTH_SHORT).show();
            Set<BluetoothDevice> pairDevices = mBlueToothAdapter.getBondedDevices();
            if (pairDevices.size() > 0) {
                for (BluetoothDevice device : pairDevices) {
                    //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                    //b.setText(device.getName().toString());

                    try {
                        BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                        socket.connect();
                        outputStream = socket.getOutputStream();
                        outputStream.write((pasteData.toString() + "\0").getBytes());
                        outputStream.flush();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }

    private void ConnectCheck() throws InvocationTargetException, IllegalAccessException //연결되었는지 검사
    {
        if (mBlueToothAdapter == null) {
            // 만약 블루투스 adapter가 없으면, 블루투스를 지원하지 않는 기기이거나 블루투스 기능을 끈 기기이다.
            Toast.makeText(getApplicationContext(), "현재 기기는 블루트스 기능을 지원하지 않습니다.", Toast.LENGTH_SHORT);
        } else {

            // 블루투스 adapter가 있으면, 블루투스 adater에서 페어링된 장치 목록을 불러올 수 있다.
            Set<BluetoothDevice> pairDevices = mBlueToothAdapter.getBondedDevices();

            //페어링된 장치가 있으면
            if (pairDevices.size() > 0) {
                for (BluetoothDevice device : pairDevices) {
                    //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                    //b.setText(device.getName().toString());
                    ParcelUuid[] uuids = device.getUuids();
                    for (ParcelUuid uuid : uuids) {
                        if (uuid.getUuid().toString().equals("9bde4762-89a6-418e-bacf-fcd82f1e0677")) {
                            try {
                                BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                                socket.connect();
                                b.setText(socket.getRemoteDevice().getName());


                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }


            }
            //페어링된 장치가 없으면
            else {
                //기본 텍스트로 버튼 텍스트를 변경한다.
                b.setText(R.string.search);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Set<BluetoothDevice> pairDevices = mBlueToothAdapter.getBondedDevices();

        //페어링된 장치가 있으면
        if (pairDevices.size() > 0) {
            for (BluetoothDevice device : pairDevices) {
                //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                //b.setText(device.getName().toString());

                try {
                    BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                    socket.connect();
                    b.setText(socket.getRemoteDevice().getName());
                    socket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //페어링된 장치가 없으면
        else{
            //기본 텍스트로 버튼 텍스트를 변경한다.
            b.setText(R.string.search);
        }
    }

    public void buttonClick(View v) {
        if (mBlueToothAdapter == null) {
            // 만약 블루투스 adapter가 없으면, 블루투스를 지원하지 않는 기기이거나 블루투스 기능을 끈 기기이다.
        } else {
            //블루투스 세팅 화면을 연다.
            Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivityForResult(intent, 0);
        }


        //브로드캐스트리시버를 이용하여 블루투스 장치가 연결이 되고, 끊기는 이벤트를 받아 올 수 있다.


        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        registerReceiver(bluetoothReceiver, filter);
        filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(bluetoothReceiver, filter);
    }

    BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            //연결된 장치를 intent를 통하여 가져온다.
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            //장치가 연결이 되었으면
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Log.d("TEST", device.getName().toString() + " Device Is Connected!");
                //b.setText(device.getName().toString());
                try {
                    BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                    socket.connect();
                    b.setText(socket.getRemoteDevice().getName());
                    socket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //연결 시작

            }
            //장치의 연결이 끊기면
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.d("TEST", device.getName().toString() + " Device Is Disconnected!");
            }
        }
    };

}