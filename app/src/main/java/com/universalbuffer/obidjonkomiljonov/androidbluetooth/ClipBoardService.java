package com.universalbuffer.obidjonkomiljonov.androidbluetooth;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by leocordius on 24/05/2017.
 */

public class ClipBoardService extends Service {
    private static final String TAG = "ClipboardManager";
    private static final String FILENAME = "clipboard-history.txt";

    private File mHistoryFile;
    private ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    private ClipboardManager mClipboardManager;
    BluetoothAdapter mBlueToothAdapter;
    NotificationService notificationService;

    @Override
    public void onCreate() {
        super.onCreate();
        mBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mClipboardManager.addPrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("Universal buffer")
                        .setOngoing(true)
                        .setContentText("We make life easier");

        Intent notificationIntent = new Intent(this, TestMainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification notification = builder.build();
//        manager.notify(0, builder.build());
        startForeground(1001, notification);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mClipboardManager != null) {
            mClipboardManager.removePrimaryClipChangedListener(
                    mOnPrimaryClipChangedListener);
        }
        super.onDestroy();


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener =
            new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {
                    Log.d(TAG, "onPrimaryClipChanged");
                    ClipData clip = mClipboardManager.getPrimaryClip();
                    mThreadPool.execute(new WriteHistoryRunnable(
                            clip.getItemAt(0).getText()));
                }
            };

    private class WriteHistoryRunnable implements Runnable {
        private final Date mNow;
        private final CharSequence mTextToWrite;
        OutputStream outputStream;

        public WriteHistoryRunnable(CharSequence text) {
            mNow = new Date(System.currentTimeMillis());
            mTextToWrite = text;
        }

        @Override
        public void run() {
            if (TextUtils.isEmpty(mTextToWrite)) {
                return;
            }

            if (isExternalStorageWritable()) {
                Log.i(TAG, "Writing new clip to history:");
                Log.i(TAG, mTextToWrite.toString());
                Set<BluetoothDevice> pairDevices = mBlueToothAdapter.getBondedDevices();
                if (pairDevices.size() > 0) {
                    for (BluetoothDevice device : pairDevices) {
                        //버튼 텍스트에 현재 디바이스 이름을 넣는다.
                        //b.setText(device.getName().toString());

                        try {
                            BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("9bde4762-89a6-418e-bacf-fcd82f1e0677"));
                            socket.connect();
                            outputStream = socket.getOutputStream();
                            outputStream.write((mTextToWrite.toString() + "\0").getBytes());
                            outputStream.flush();
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if(outputStream != null)
                                    outputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                Log.w(TAG, "External storage is not writable!");
            }
        }
    }
}